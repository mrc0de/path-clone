import os, shutil, errno, sqlite3, time

def connect_db(dbname):
	try:
		dbconn = sqlite3.connect(dbname)
		#cursor = dbconn.cursor()
	except:
		print ("error connecting to db")

	return dbconn

def path_clone(source_dir_prompt, destination_dir_prompt, symlinks=False, ignore=None):
	src = source_dir_prompt
	dst = destination_dir_prompt
	if not os.path.exists(dst):
		os.makedirs(dst)
	for item in os.listdir(src):
		s = os.path.join(src, item)
		d = os.path.join(dst, item)
		if os.path.isdir(s):
			copytree(s, d, symlinks, ignore)
		else:
			if not os.path.exists(d) or os.stat(s).st_mtime - os.stat(d).st_mtime > 1:
				shutil.copy2(s, d)

def use_specific_path(id):
	dbconn = sqlite3.connect('./syncdb.db')
	cursor = dbconn.cursor()
	specific_path = cursor.execute(''' SELECT * FROM paths WHERE id = ? ''', (id,) )
	specific_row = cursor.fetchall()
	print("got here. [1]")
	print(specific_row)

def show_paths():
	#try:
	dbconn = sqlite3.connect('./syncdb.db')
	cursor = dbconn.cursor()
	paths = cursor.execute(''' SELECT * FROM paths ''')
	rows = cursor.fetchall()
	print("got here. [0.5]")
	print(rows)


def clone_saved_path():
	show_your_paths = input('Do you want to view your current paths? [1] Yes [2] No ')
	show_your_paths = int(show_your_paths)
	if show_your_paths == 1:
		show_paths()
		path_chosen_to_clone = input('Which path would you like to clone? Enter a number ')
		path_chosen_to_clone = int(path_chosen_to_clone)
		
		for p in use_specific_path(path_chosen_to_clone) :
			src = str(p[1]) #get [source]
			dst = str(p[2]) #get [destination]

			try:
				#if path already exists, remove it before copying with copytree()
				if os.path.exists(dst):
					shutil.rmtree(dst)

				shutil.copytree(src, dst)
				#print("Potentially copied?")
			except OSError as e:
				# If the error was caused because the source wasn't a directory
				if e.errno == errno.ENOTDIR:
					shutil.copy(source_dir_prompt, destination_dir_prompt)
				else:
					print('Directory not copied. Error: %s' % e)
#instantiate
clone_saved_path()
'''
Path Clone is a dropbox replacement for the nerdy-types who rather backup files via command line, than using a gui. 
Plus, GUIs can be slower :)

We're using a sqlite db here to store paths you'd like to save, so backing up in the future is an easy as pie.

@author : Joseph Dickinson, 2015.
'''
## necessary imports
import os, shutil, errno, sqlite3, time, pprint

#pretty print settings
pp = pprint.PrettyPrinter(indent=1)

#METHODS

"""
path_clone() takes (2) STRING parameters: a source directory to copy from, and a destination directory
to copy to. You may additionally add files to ignore, via the 'ignore' parameter.
"""
def path_clone(source_dir_prompt, destination_dir_prompt, symlinks=False, ignore=None):
	src = source_dir_prompt
	dst = destination_dir_prompt
	if not os.path.exists(dst):
		os.makedirs(dst)
	for item in os.listdir(src):
		s = os.path.join(src, item)
		d = os.path.join(dst, item)
		if os.path.isdir(s):
			copytree(s, d, symlinks, ignore)
		else:
			if not os.path.exists(d) or os.stat(s).st_mtime - os.stat(d).st_mtime > 1:
				shutil.copy2(s, d)

"""
use_specific_path() takes (1) INTEGER parameter: which is the primary key of the path saved to the sqlite3
database. This number is easily found by selecting option 2 for show_paths() to show all of your paths in 
the database. 
"""
def use_specific_path(id):
	dbconn = sqlite3.connect('./syncdb.db')
	cursor = dbconn.cursor()
	specific_path = cursor.execute(''' SELECT * FROM paths WHERE id = ? ''', (id,) )
	specific_row = cursor.fetchall()
	return specific_row

"""
create_path() takes (0) parameters: It serves a primary purpose of creating new source/destination STRING
combinations to copy from and to. 
"""
def create_path():
	dbconn = sqlite3.connect('./syncdb.db')
	cursor = dbconn.cursor()
	cursor.execute('''
		CREATE TABLE IF NOT EXISTS paths (
		id INTEGER PRIMARY KEY AUTOINCREMENT, 
		source TEXT, 
		destination TEXT
		) 
	''')
	#cursor.execute(''' CREATE UNIQUE INDEX src ON paths(source); ''');
	#cursor.execute(''' CREATE UNIQUE INDEX dest ON paths(destination); ''');
	dbconn.commit()
	
	print("Database table created")
	
	time.sleep(2)

	new_source_dir = input('What directory to copy from? ')
	new_destination_dir = input('What directory to copy to? ')

	try:
		# @TODO make this gracefully handle a duplicate row...currently throwing an error to user.
		cursor.execute('''
			INSERT or IGNORE INTO
			paths(source, destination) 
			VALUES(?, ?)''',
			(str(new_source_dir), str(new_destination_dir))
		)
		dbconn.commit()

	except sqlite3.OperationalError:
		print("Can't save path to db")
		pass
	except IntegrityError:
		print("That path already exists")
		pass
	print ("*~*~*~*~*~*~*~*~*~*~*~*~*~\ncommitted your paths to database\n ...now closing database.\n*~*~*~*~*~*~*~*~*~*~*~*~*~")
	dbconn.close()

"""
show_paths() takes (0) parameters: It serves a primary purpose of showing all of the paths within the sqlite3
database to copy against. 
"""
def show_paths():
	#try:
	dbconn = sqlite3.connect('./syncdb.db')
	cursor = dbconn.cursor()
	paths = cursor.execute(''' SELECT * FROM paths ''')
	rows = cursor.fetchall()
	pp.pprint(rows)

#TODO delete_path()

"""
clone_saved_path() takes (0) parameters: It serves a primary purpose of taking in a specific path to clone against. 
It then will delete the destination directory if it exists. Then it will do a recursive copy of the files in 
source to the destination directory. 
"""
def clone_saved_path():
	print("Your paths\n___________________________________________________________________________________________________\n")
	show_paths()
	print("___________________________________________________________________________________________________\n")
	path_chosen_to_clone = input('Which path would you like to clone? Enter a number: ')
	path_chosen_to_clone = int(path_chosen_to_clone)
		
	for p in use_specific_path(path_chosen_to_clone) :
		src = str(p[1]) #get [source]
		dst = str(p[2]) #get [destination]

		try:
			#if path already exists, remove it before copying with copytree()
			if os.path.exists(dst):
				shutil.rmtree(dst)
			shutil.copytree(src, dst)
			print("Copying files...")
			time.sleep(2)
		except OSError as e:
			# If the error was caused because the source wasn't a directory
			if e.errno == errno.ENOTDIR:
				shutil.copy(src, dst)
			else:
				print('Directory not copied. Error: %s' % e)

"""
sync() takes (0) parameters: It serves a primary purpose of setting up the user interface on CLI. It has (3) paths 
1) - create a new path.
2) - show all the paths in the database.
3) Perform a clone against an already saved path in the database.
"""
def sync():
	to_do = input("What would you like to do? [1] Insert New Path [2] Show Paths [3] Use a Path ")
	to_do = int(to_do)
	if to_do == 1:
		create_path()
	if to_do == 2:
		show_paths()
	if to_do == 3:
		clone_saved_path()

sync()

#@TODO schedule_sync()
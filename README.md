## Path Clone
### By Joseph Dickinson, 2015


```
Path Clone is a CLI(Command-Line-Interface) driven application consisting of several functions
that allow a user to easily keep track of 1:M source:destination combinations to copy 
from and to. 
```
```
Path Clone was born out of the frustration of constantly being worried as a developer if I was
going to lose all of the code I've been so diligently working to develop over time. 

Its purpose is to make your file syncing needs simple, yet effective so you as a developer, 
designer, or creator can sleep better at night.
```

####Path Clone was built on the premises of :####
- Keep the process as **simple**, and **straight-forward** as possible.
- File syncing should be **light-weight** and should **just work** instead of frustrating you.
- Developers, and creators alike just need **effective, and efficient** backup between (2) paths.
1. Adding a new path: should be as **simple as typing (2) paths**, no more.
  1. **Run** with: `python path_clone.py`, **choose** option 2, then **type** `source` and `destination`.
- Removing an old path: should be as simple as typing 1 command.

####Installation/Use####
**Installation:**
- Install virtualenv (`pip install virtualenv`)
- Install Python 3+ (depends on your `os`)

**Use:**
- Run: `python path_clone.py` (preferably within a virtualenv)
- Choose an option

**Note from author:** I hope you enjoy this program, as much as I enjoyed architecting and writing it. 

**Questions?** - let's talk: jpd 750 [at] gmail.com (*remove spacing and []*)
